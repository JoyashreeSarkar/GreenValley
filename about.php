<!doctype html>
<html lang="en">
<?php include ("head.php"); ?>
<body>

<!--Pre Loader-->
<div class="pre-loader"><div class="loader"></div></div>



<?php include ("header.php"); ?>

</hr>


<div class="container">
    <div class="col-sm-4 col-md-4">
		<h2 style="color:#3399CC">ABOUT US</h2>

	</div>
	
	<div class="col-sm-8 col-md-8">
		<div class="r">
			<p><b>GREEN VALLEY is an online shop whose mission is to help people achieve a higher quality of life through the attainment of people's health & wellness goals.</b></p>

			<p>We believe being healthy is a journey, there’s no express way to achieve, no one stop shop product for it. You need to achieve ‘health’ in multiple categories & it doesn’t come without work & determination. It usually takes a few steps forward & a couple back repeated over & over.</p> 

			<p>At GREEN VALLEY, we look at creative ways to deliver consumer organic food that enhance the everyday lives of our customers. We do this by providing an exclusive line of natural health products that aim to Cleanse, Nourish & Energise one mouthful, swirl or sip at a time!</p>
			
			<p>We strive to lead from the front within the industry & achieve this by sourcing & selecting only the highest quality products & ingredients from the finest suppliers globally.</p>
			
			<p>GREEN VALLEY has proudly assisted thousands of people just like you to achieve their wellness goals, it is this success that keeps our customers coming back & better yet inspires us to keep developing & growing so we are able to continually provide the finest products & services possible!</p>
			
			<p>To get to know Christine and the team behind GREEN VALLEY, contact us by <a href="contact.html">clicking here..</a></p>
		</div>
	</div>
	
</div>





<?php include ("footer.php"); ?>





<!--This is JQUARY -->
<script src="js/jquery-3.2.1.min.js"></script>

<!--This is Bootstrap-4 JS-->
<script src="js/bootstrap.min.js"></script>





<!--Page Loader-->
<script>
    setTimeout(function () {
        $('.pre-loader').fadeToggle();
    }, 1500);
</script>
</body>
</html>