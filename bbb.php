<!doctype html>
<html lang="en">
<?php include ("head.php"); ?>
    <body>


	    <!-- Navbar-->
   <?php include ("header.php"); ?>

    <div class="main-wrapper">

        <div id="vue">
            <div class="cart"><span class="cart-size"> 0 </span><i class="fa fa-shopping-cart"></i>
				<div class="cart-items" style="display: none;">
				<table class="cartTable"><tbody></tbody></table>
				<h4 class="cartSubTotal" style="display: none;"> $0.00 </h4>
				<button class="clearCart" style="display: none;"> Clear Cart </button>
				<button class="checkoutCart" style="display: none;"> Checkout </button>
				<table></table>
				</div>
			</div>
			
			
            <h1 style="color: #4a235a ">Shopping Cart</h1><br>
			<p style="font-size:20px"><i>There are no items in your cart.<a href="http://localhost/OJIR&#39;s%20Green%20Valley/index.php">Continue Shopping → </a></i></p>
			
			
			<div class="products">
				<div class="product apple">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/apples.jpg&quot;);">
					</div>
					<div class="name">Apple</div>
					<div class="description">lorem</div>
					<div class="price">$5.50</div><button>Add to Cart</button><br><br>
				</div>
				<div class="product apricots">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/apricots.jpg&quot;);">
					</div>
					<div class="name">apricots</div>
					<div class="description">This is a kitten</div>
					<div class="price">$10.00</div><button>Add to Cart</button><br><br>
				</div>
				<div class="product avocado">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/avocado.jpg&quot;);"></div>
					<div class="name">avocado</div><div class="description">This is a shark</div><div class="price">$15.00</div>
					<button>Add to Cart</button><br><br>
				</div>
				<div class="product berries">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/berries.jpg&quot;);"></div>
					<div class="name">berries</div>
					<div class="description">This is a puppy</div>
					<div class="price">$5.00</div>
					<button>Add to Cart</button><br><br>
				</div>
				<div class="product banana">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/banana.png&quot;);"></div>
					<div class="name">banana</div>
					<div class="description">This is an apple</div>
					<div class="price">$1.00</div>
					<button>Add to Cart</button><br><br>
				</div>
				<div class="product orange">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/orange.jpg&quot;);"></div>
					<div class="name">Orange</div>
					<div class="description">This is an orange</div>
					<div class="price">$1.10</div>
					<button>Add to Cart</button><br><br>
				</div>
				
				<div class="product peach">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/peach.jpg&quot;);"></div>
					<div class="name">Peach</div>
					<div class="description">This is a peach</div><div class="price">$1.50</div>
					<button>Add to Cart</button><br><br>
				</div>
				
				<div class="product mango">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;https://s3-us-west-2.amazonaws.com/s.cdpn.io/241793/mango.png&quot;);"></div>
					<div class="name">Mango</div>
					<div class="description">This is a mango</div>
					<div class="price">$2.00</div>
					<button>Add to Cart</button><br><br>
				</div>
				<div class="product pomegranate">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/pomegranate.jpg&quot;);"></div>
					<div class="name">pomegranate</div>
					<div class="description">This is a glass of cognac</div>
					<div class="price">$17.38</div>
					<button>Add to Cart</button><br><br>
				</div>
				<div class="product pears">
					<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;images/fruits/pears.jpg&quot;);"></div>
					<div class="name">pears</div>
					<div class="description">This is a chain</div>
					<div class="price">$17.38</div>
					<button>Add to Cart</button><br><br>
				</div>
			</div>
			<div class="modalWrapper" style="display: none;">
				<div class="prevProduct"><i class="fa fa-angle-left"></i></div>
				<div class="nextProduct"><i class="fa fa-angle-right"></i></div>
				<div class="overlay"></div>
				<div class="modal"><i class="close fa fa-times"></i>
					<div class="imageWrapper">
						<div class="image" style="background-size: cover; background-position: center center; background-image: url(&quot;undefined&quot;);"></div>
					</div>
					<div class="name"></div>
					<div class="description"></div>
					<div class="details"></div>
					<h3 class="price"></h3>
					<label for="modalAmount">QTY</label>
					<input id="modalAmount" class="amount" value="1">
					<button>Add to Cart</button>
				</div>
			</div>
            
        </div>
    </div>




    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/cart.js"></script>
    </body>
</html>