<!doctype html>
<html lang="en">

<?php include ("head.php"); ?>

<body>


<?php include ("header.php"); ?>
<!--shop start-->
<section>
    <div class="container">
        <h2 class="text-center shop-heading">VIEW ALL FRUITS</h2>
        <div class="row">

			<div class="hover04 column">

				<div class="col-md-4">
					<figure><img src="images/fruits/mangoes.jpg" />  </figure>

					<!-- Button trigger modal -->
					<div>
					<a href="addtocart.php" class="btn blue">QUICK SHOP</a>
					</div>
				</div>

				<div class="col-md-4">
					<figure><img src="images/fruits/kiwi.jpg" /></figure>
					<div>
						<a href="addtocart.php" class="btn blue">QUICK SHOP</a>
					</div>
				</div>
				<div class="col-md-4">
					<figure><img src="images/fruits/guavas.jpg" /></figure>
					<div>
						<a href="addtocart.php" class="btn blue">QUICK SHOP</a>
					</div>
				</div>

			</div>
        </div>
        <div class="row">
            <div class="hover04 column">

                <div class="col-md-4">
                    <figure><img src="images/fruits/avocado.jpg" /></figure>
                    <!-- Button trigger modal -->
                    <div>
                        <a href="addtocart.php" class="btn blue">QUICK SHOP</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <figure><img src="images/fruits/apples.jpg" /></figure>
                    <div>
                        <a href="addtocart.php" class="btn blue">QUICK SHOP</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <figure><img src="images/fruits/apricots.jpg" /></figure>
                    <div>
                        <a href="addtocart.php" class="btn blue">QUICK SHOP</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="hover04 column">

                <div class="col-md-4">
                    <figure><img src="images/fruits/berries.jpg" /></figure>
                    <!-- Button trigger modal -->
                    <div>
                        <a href="addtocart.php" class="btn blue">QUICK SHOP</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <figure><img src="images/fruits/blueberries.jpg" /></figure>
					
                    <div>
					
                        <a href="addtocart.php" class="btn blue" >QUICK SHOP</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <figure><img src="images/fruits/grapes.jpg" /></figure>
                    <div>
                        <a href="addtocart.php" class="btn blue">QUICK SHOP</a>
                    </div>

				</div>
            </div>

            </div>
        </div>
    </div>
</section>



<?php include ("footer.php"); ?>

    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	

    </body>
</html>