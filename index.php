<!doctype html>
<html lang="en">

<?php include ("head.php"); ?>


<body>


<?php include ("header.php"); ?>


<section class="banner">
	<div class="container ">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1" ></li>
				<li data-target="#myCarousel" data-slide-to="2" ></li>
				<li data-target="#myCarousel" data-slide-to="3" ></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="images/vegetables.jpg" alt="pic1" style="width:100%; " >
				</div>

				<div class="item">
					<img src="images/b2.jpg" alt="pic2" style="width:100% " >
				</div>

				<div class="item">
					<img src="images/b3.png" alt="pic3" style="width:100%" >
				</div>
				<div class="item">
					<img src="images/b4.jpg" alt="pic4" style="width:100%" >
				</div>

			</div>
		</div>

			 <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
	</div>

</section>


<?php include ("footer.php"); ?>
 
<!--This is JQUARY -->
<script src="js/jquery-3.2.1.min.js"></script>

<!--This is Bootstrap-4 JS-->
<script src="js/bootstrap.min.js"></script>



<!--Page Loader-->
<script>
    setTimeout(function () {
        $('.pre-loader').fadeToggle();
    }, 1500);
</script>
</body>
</html>